module.exports = {
    testMatch: ["<rootDir>/src/test/*.spec.ts", "<rootDir>/src/test/**/*.spec.ts"],
    preset: 'ts-jest',
    testEnvironment: 'node',
    transform: {
        '^.+\\.ts?$': 'ts-jest',
    },
    transformIgnorePatterns: ['<rootDir>/node_modules/'],
};