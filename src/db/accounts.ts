import { Err, Ok, Result } from "ts-results";
import { appendRecord, getTodayAmount, RecordKind } from "./records";

export class Account {
  private readonly maxDepositPerDay = 5000;
  private totalAmount: number;

  constructor(public accountId: number, testingInitAmount?: number) {
    this.totalAmount = testingInitAmount || 0;
  }

  withdraw(amount: number): Result<void, Error> {
    if (amount < 0) {
      return Err(new Error("Negative amount received"));
    }

    const diff = this.totalAmount - amount;

    if (diff < -200) {
      return Err(new Error("Amount too big to withdraw"));
    }

    appendRecord(RecordKind.WITHDRAW, amount, this.accountId);
    this.totalAmount -= amount;

    return Ok.EMPTY;
  }

  deposit(amount: number): Result<void, Error> {
    if (amount < 0) {
      return Err(new Error("Negative amount received"));
    }

    const todayAcum = getTodayAmount([RecordKind.DEPOSIT, RecordKind.TRANSFERIN], this.accountId);
    const totalToday = todayAcum + amount;

    if (totalToday > this.maxDepositPerDay) {
      return Err(new Error("Daily limit exceeded"));
    }

    appendRecord(RecordKind.DEPOSIT, amount, this.accountId);
    this.totalAmount += amount;

    return Ok.EMPTY;
  }

  ensureTransferOut(amount: number): Result<void, Error> {
    if (amount < 0) {
      return Err(new Error("Negative amount received"));
    }

    const diff = this.totalAmount - amount;

    if (diff < 0) {
      return Err(new Error("Amount too big to transfer"));
    }

    return Ok.EMPTY;
  }

  ensureTransferIn(amount: number): Result<void, Error> {
    if (amount < 0) {
      return Err(new Error("Negative amount received"));
    }

    const todayAcum = getTodayAmount([RecordKind.DEPOSIT, RecordKind.TRANSFERIN], this.accountId);
    const totalToday = todayAcum + amount;

    if (totalToday > this.maxDepositPerDay) {
      return Err(new Error("Daily limit exceeded"));
    }

    return Ok.EMPTY;
  }

  performTransferOut(amount: number): void {
    appendRecord(RecordKind.TRANSFEROUT, amount, this.accountId);
    this.totalAmount -= amount;
  }

  performTransferIn(amount: number): void {
    appendRecord(RecordKind.TRANSFERIN, amount, this.accountId);
    this.totalAmount += amount;
  }

  /** Only for testing purposes */
  _getCurrentAmount(): number {
    return this.totalAmount;
  }
}

const accounts: Array<Account> = [ new Account(1, 400), new Account(2) ];

export const findAccount = (accountId: number) => {
  return accounts.find((account) => account.accountId === accountId);
};

/** Exported for testing purposes */
export const _accounts = accounts