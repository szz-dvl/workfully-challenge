import { Err, Ok, Result } from "ts-results";
import { Account } from "./accounts";

export class Transfer {
  constructor(private fromAccount: Account, private toAccount: Account) {}

  /** Public for testing purposes only  */
  ensure(amount: number): Result<void, Error> {
    const resultOut = this.fromAccount.ensureTransferOut(amount);

    if (resultOut.err) {
      return resultOut;
    }

    const resultIn = this.toAccount.ensureTransferIn(amount);

    if (resultIn.err) {
      return resultIn;
    }

    return Ok.EMPTY;
  }

  perform(amount: number): Result<void, Error> {

    const result = this.ensure(amount)

    if (result.err) {
        return result
    }

    this.fromAccount.performTransferOut(amount);
    this.toAccount.performTransferIn(amount);

    return Ok.EMPTY;
  }
}

export const transfer = ({
    origin,
    dest, 
    amount
}:{
  origin: Account,
  dest: Account,
  amount: number,
}): Result<void, Error> => {
  
  return new Transfer(origin, dest).perform(amount)

};