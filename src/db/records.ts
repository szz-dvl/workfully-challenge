import {DateTime} from "luxon";

const records: Array<Record> = []

export enum RecordKind {
    WITHDRAW,
    DEPOSIT,
    TRANSFERIN,
    TRANSFEROUT
}

export class Record {

    public date: Date;

    constructor(public kind: RecordKind, public amount: number, public accountId: number) {
        this.date = new Date();
    }
}

/** Exported for testing purposes */
export const todayStart = (ts: DateTime): number => {
    const todayStart = ts.startOf("day")
    return todayStart.toMillis()
}

/** Exported for testing purposes */
export const todayEnd = (ts: DateTime): number => {
    const todayEnd = ts.endOf("day")
    return todayEnd.toMillis()
}

export const getTodayAmount = (kind: Array<RecordKind>, accountId: number): number => {

    const nowTs = DateTime.now();
    const startOfDay = todayStart(nowTs);
    const endOfDay = todayEnd(nowTs);

    return records.reduce((agg, { accountId: currentAccountId, kind: currentKind, date, amount }) => {
        
        if (currentAccountId !== accountId)
            return agg;

        if (!kind.includes(currentKind))
            return agg;

        const ts = date.getTime();

        if (ts < startOfDay || ts > endOfDay)
            return agg

        return agg + amount;

    }, 0);
}

export const appendRecord = (kind: RecordKind, amount: number, accountId: number): void => {
    records.push(new Record(kind, amount, accountId))
}

/** Exported for testing purposes */
export const _records = records