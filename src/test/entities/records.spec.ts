import { DateTime } from "luxon";
import { appendRecord, getTodayAmount, Record, RecordKind, todayEnd, todayStart, _records } from "../../db/records";

describe('Testing for Record entity', () => {
    it('should be OK', () => {
        expect(true).toBe(true);
    });
    it('should create an empty record instance', async () => {
        const {date, ...record} = new Record(RecordKind.DEPOSIT, 100, 1);

        expect(record).toMatchInlineSnapshot(`
{
  "accountId": 1,
  "amount": 100,
  "kind": 1,
}
`);
    }); 

    describe("todayStart", () => {
        it('should return the timestamp for the start of the current day', () => {
            const millis = todayStart(DateTime.now())

            expect(millis).toBe(DateTime.fromJSDate(new Date()).startOf("day").toMillis())

        });
    })

    describe("todayEnd", () => {
        it('should return the timestamp for the end of the current day', () => {
            const millis = todayEnd(DateTime.now())

            expect(millis).toBe(DateTime.fromJSDate(new Date()).endOf("day").toMillis())

        });
    })

    describe("appendRecord", () => {
        it('should append a new record in the register', () => {

            _records.length = 0;
            appendRecord(RecordKind.DEPOSIT, 100, 1)

            expect(_records).toHaveLength(1)

        });
    })

    describe("getTodayAmount", () => {
        it('should compute the total daily amount for records of a given kind and account', () => {

            _records.length = 0;
            appendRecord(RecordKind.DEPOSIT, 200, 1)
            appendRecord(RecordKind.DEPOSIT, 300, 1)
            appendRecord(RecordKind.TRANSFERIN, 500, 1)

            const result = getTodayAmount([RecordKind.DEPOSIT, RecordKind.TRANSFERIN], 1)

            expect(result).toBe(1000)

        });
    })

});