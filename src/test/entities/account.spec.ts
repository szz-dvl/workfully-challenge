import { Err } from "ts-results";
import { Account, findAccount, _accounts } from "../../db/accounts";
import { appendRecord, RecordKind, _records } from "../../db/records";

describe('Testing for Account entity', () => {
    it('should be OK', () => {
        expect(true).toBe(true);
    });
    it('should create an empty account instance', async () => {
        const account = new Account(1);

        expect(account._getCurrentAmount()).toBe(0);
    });

    describe("withdraw", () => {
        it('should refuse to withdraw money for a negative quantity', async () => {
            const account = new Account(1, 200);

            const result = account.withdraw(-100) as Err<Error>;
    
            expect(result.val.message).toBe("Negative amount received")
        });

        it('should refuse to withdraw money for a quantity above the limit', async () => {
            const account = new Account(1, 200);

            const result = account.withdraw(600) as Err<Error>;
    
            expect(result.val.message).toBe("Amount too big to withdraw")
        });

        it('should withdraw money for a valid quantity', async () => {
            const account = new Account(1, 200);

            const result = account.withdraw(100);
    
            expect(account._getCurrentAmount()).toBe(100)
        });
    })

    describe("deposit", () => {
        it('should refuse to deposit money for a negative quantity', async () => {
            const account = new Account(1);

            const result = account.deposit(-100) as Err<Error>;
    
            expect(result.val.message).toBe("Negative amount received")
        });

        it('should refuse to deposit money when exceeding the daily limit', async () => {
            const account = new Account(1);
            appendRecord(RecordKind.DEPOSIT, 500, 1)
            appendRecord(RecordKind.DEPOSIT, 2000, 1)

            const result = account.deposit(3000) as Err<Error>;
    
            expect(result.val.message).toBe("Daily limit exceeded")
        });

        it('should deposit money for a valid quantity', async () => {
            const account = new Account(1);

            const result = account.deposit(100);
    
            expect(account._getCurrentAmount()).toBe(100)
        });
    })

    describe("ensureTransferOut", () => {
        it('should refuse to ensure an outgouing transference for a negative quantity', async () => {
            const account = new Account(1);

            const result = account.ensureTransferOut(-100) as Err<Error>;
    
            expect(result.val.message).toBe("Negative amount received")
        });

        it('should refuse to ensure an outgouing transference when exceeding the account total amount', async () => {
            const account = new Account(1, 200);
            
            const result = account.ensureTransferOut(300) as Err<Error>;
    
            expect(result.val.message).toBe("Amount too big to transfer")
        });

        it('should ensure an outgouing transference for a valid quantity', async () => {
            const account = new Account(1, 200);

            const result = account.ensureTransferOut(100);
    
            expect(result.ok).toBeTruthy();
        });
    })

    describe("ensureTransferIn", () => {
        it('should refuse to ensure an incoming transference for a negative quantity', async () => {
            const account = new Account(1);

            const result = account.ensureTransferIn(-100) as Err<Error>;
    
            expect(result.val.message).toBe("Negative amount received")
        });

        it('should refuse to ensure an incoming transference for a quantity exceeding the daily limit', async () => {
            const account = new Account(1);
            
            _records.length = 0;
            appendRecord(RecordKind.DEPOSIT, 200, 1)
            appendRecord(RecordKind.DEPOSIT, 300, 1)
            appendRecord(RecordKind.TRANSFERIN, 500, 1)

            const result = account.ensureTransferIn(4200) as Err<Error>;
    
            expect(result.val.message).toBe("Daily limit exceeded")
        });

        it('should ensure an incoming transference for a valid quantity', async () => {
            const account = new Account(1, 200);

            const result = account.ensureTransferIn(100);
    
            expect(result.ok).toBeTruthy();
        });
    })

    describe("performTransferOut", () => {
        it('should actually perform an outgoing transaction', async () => {
            const account = new Account(1, 200);

            const result = account.performTransferOut(100);
    
            expect(account._getCurrentAmount()).toBe(100)
        });
    })

    describe("performTransferIn", () => {  
        it('should actually perform an incoming transaction', async () => {
            const account = new Account(1, 200);

            const result = account.performTransferIn(100);
    
            expect(account._getCurrentAmount()).toBe(300)
        });
    })

    describe("findAccount", () => {  
        it('should find an existing account', async () => {
            _accounts.length = 0;
            _accounts.push(new Account(1));

            const result = findAccount(1);
    
            expect(result).toBeDefined()
        });

        it('should not find a not existing account', async () => {
            _accounts.length = 0;
            _accounts.push(new Account(1));

            const result = findAccount(2);
    
            expect(result).toBeUndefined()
        });
    })
});
