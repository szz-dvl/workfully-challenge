import { Account } from "../../db/accounts";
import { Transfer } from "../../db/transfers";

describe('Testing for Transfer entity', () => {
    it('should be OK', () => {
        expect(true).toBe(true);
    });
    it('should create an empty transfer instance', async () => {
        const transfer = new Transfer(new Account(1), new Account(2));

        expect(transfer).toMatchInlineSnapshot(`
Transfer {
  "fromAccount": Account {
    "accountId": 1,
    "maxDepositPerDay": 5000,
    "totalAmount": 0,
  },
  "toAccount": Account {
    "accountId": 2,
    "maxDepositPerDay": 5000,
    "totalAmount": 0,
  },
}
`);
    });

    describe("ensure", () => {
        it('should refuse to ensure a transference for an invalid amount', async () => { 
            const transfer = new Transfer(new Account(1), new Account(2));

            const result = transfer.ensure(300)

            expect(result.err).toBeTruthy()
        });

        it('should ensure a transference for a valid amount', async () => { 
            const transfer = new Transfer(new Account(1, 600), new Account(2));

            const result = transfer.ensure(300)

            expect(result.ok).toBeTruthy()
        });
    })

    describe("perform", () => {
        it('should refuse to perform a transference for an invalid amount', async () => { 
            const transfer = new Transfer(new Account(1), new Account(2));

            const result = transfer.perform(300)

            expect(result.err).toBeTruthy()
        });

        it('should perform a transference for a valid amount', async () => { 
            const accountDest = new Account(2)
            const transfer = new Transfer(new Account(1, 600), accountDest);

            const result = transfer.perform(300)

            expect(accountDest._getCurrentAmount()).toBe(300)
        });
    })

    /** Transfer function is not added to tests because is only a wrapper arround the previous "perform" function */

})