import request from "supertest";
import { App } from "../../app/App";

export const registerDeposit = (app: App) => {
  describe("Deposit Test", () => {
    it("should respond 404 for an unexistent account", async () => {
      const response = await request(app.express).post("/deposit").send({
        accountId: 3,
        amount: 200,
      }).set("Content-Type", "application/json")
        .set("Accept", "application/json");
  
      expect(response.status).toBe(404);
    });
  
    it("should respond 500 for an invalid amount", async () => {
      const response = await request(app.express).post("/deposit").send({
        accountId: 1,
        amount: -200,
      }).set("Content-Type", "application/json")
        .set("Accept", "application/json");
  
      expect(response.status).toBe(500);
    });
  
    it("should respond 200 for a valid account", async () => {
      const response = await request(app.express).post("/deposit").send({
        accountId: 1,
        amount: 200,
      }).set("Content-Type", "application/json")
        .set("Accept", "application/json");
  
      expect(response.status).toBe(200);
    });
  });
}

it("should be OK", () => {
  expect(true).toBe(true);
});


// afterAll((done) => {
//   app.stop();
//   done();
// });
