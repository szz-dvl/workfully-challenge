import { registerDeposit } from "./deposit.spec";
import { registerWithdraw } from "./withdraw.spec";
import { registerTransfer } from "./transfer.spec";
import app from "../../app/main";

registerDeposit(app);
registerWithdraw(app);
registerTransfer(app);

afterAll((done) => {
  app.stop();
  done();
});
