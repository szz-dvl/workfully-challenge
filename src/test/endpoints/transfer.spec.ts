import request from "supertest";
import { App } from "../../app/App";

export const registerTransfer = (app: App) => {
  describe("Transfer Test", () => {
    it("should respond 404 for an unexistent account", async () => {
      const response = await request(app.express).post("/transfer").send({
        fromAccountId: 3,
        toAccountId: 1,
        amount: 200,
      }).set("Content-Type", "application/json")
        .set("Accept", "application/json");

      expect(response.status).toBe(404);
    });

    it("should respond 400 for the same id on origin and destination accounts", async () => {
      const response = await request(app.express).post("/transfer").send({
        fromAccountId: 1,
        toAccountId: 1,
        amount: 200,
      }).set("Content-Type", "application/json")
        .set("Accept", "application/json");

      expect(response.status).toBe(400);
    });

    it("should respond 500 for an invalid amount", async () => {
      const response = await request(app.express).post("/transfer").send({
        fromAccountId: 2,
        toAccountId: 1,
        amount: -200,
      }).set("Content-Type", "application/json")
        .set("Accept", "application/json");

      expect(response.status).toBe(500);
    });

    it("should respond 200 for a valid account", async () => {
      const response = await request(app.express).post("/transfer").send({
        fromAccountId: 1,
        toAccountId: 2,
        amount: 200,
      }).set("Content-Type", "application/json")
        .set("Accept", "application/json");

      expect(response.status).toBe(200);
    });
  });
};


it("should be OK", () => {
  expect(true).toBe(true);
});