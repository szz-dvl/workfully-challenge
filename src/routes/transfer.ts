import express from "express";
import { findAccount } from "../db/accounts";
import { transfer } from "../db/transfers";

const router = express.Router();

router.post(
  "/",
  async ({ body: { fromAccountId, toAccountId, amount } }, res, next) => {
    try {
      if (fromAccountId === toAccountId) {
        return res.status(400).json({
          status: false,
          message: "Nothing to transfer",
        });
      }

      const origin = findAccount(fromAccountId);

      if (!origin) {
        return res.status(404).json({
          status: false,
          message: `Account "${fromAccountId}" not found.`,
        });
      }

      const dest = findAccount(toAccountId);

      if (!dest) {
        return res.status(404).json({
          status: false,
          message: `Account "${toAccountId}" not found.`,
        });
      }

      const result = transfer({
        origin,
        dest,
        amount,
      });

      if (result.err) {
        return res.status(500).json({
          status: false,
          message: result.val.message,
        });
      }

      return res.status(200).json({
        status: true,
      });
    } catch (err) {
      next(err);
    }
  },
);

export default router;
