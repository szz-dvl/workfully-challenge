import express from "express";
import { findAccount } from "../db/accounts";

const router = express.Router();

router.post("/", async ( { body: { accountId, amount }}, res, next) => {

    try {

        const account = findAccount(accountId)

        if (!account) {
            return res.status(404).json({
                status: false,
                message: `Account "${accountId}" not found.`
            });
        }

        const result = account.withdraw(amount)

        if (result.err) {
            return res.status(500).json({
                status: false,
                message: result.val.message
            })
        }

        return res.status(200).json({
            status: true
        });

    } catch(err) {
        next(err)
    }
    
});

export default router;