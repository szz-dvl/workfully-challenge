export const spec = `
openapi: 3.0.0
info:
  title: APi for Workfully candidature
  description: Bank Account API
  termsOfService: https://terms.com/terms-of-service/resource.json
  contact:
    name: Santi Martínez Pérez
    url: https://github.com/szz-dvl
    email: santiaguzz@gmail.com
  version: "1"
servers:
  - description: Local server
    url: http://localhost:3300
paths:
  /deposit:
    summary: Deposit money
    description: Deposit money
    post:
      tags:
        - deposit
      summary: Deposit money
      description: Deposit money
      operationId: deposit
      requestBody:
        description: Account and amount to be deposited
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                accountId: 
                  type: number
                amount: 
                  type: number
              additionalProperties: false
              example: { "accountId": 1, "amount": 200 }
      responses:
        "200":
          description: Deposit operation completed successfully
          content:
            application/json:
              schema:
                type: object
                properties:
                  success: 
                    type: boolean
        "404":
          description: Account not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  success: 
                    type: boolean
                  message: 
                    type: string
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UnexpectedError"
      deprecated: false
  /withdraw:
    summary: Withdraw money
    description: Withdraw money
    post:
      tags:
        - withdraw
      summary: Withdraw money
      description: Withdraw money
      operationId: withdraw
      requestBody:
        description: Account and amount to be withdrawed
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                accountId: 
                  type: number
                amount: 
                  type: number
              additionalProperties: false
              example: { "accountId": 1, "amount": 200 }
      responses:
        "200":
          description: Withdraw operation completed successfully
          content:
            application/json:
              schema:
                type: object
                properties:
                  success: 
                    type: boolean
        "404":
          description: Account not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  success: 
                    type: boolean
                  message: 
                    type: string
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UnexpectedError"
      deprecated: false
  /transfer:
    summary: Withdraw money
    description: Withdraw money
    post:
      tags:
        - transfer
      summary: Transfer money
      description: Transfer money
      operationId: transfer
      requestBody:
        description: Account and amount to be transfered
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                fromAccountId: 
                  type: number
                toAccountId: 
                  type: number
                amount: 
                  type: number
              additionalProperties: false
              example: { "fromAccountId": 1, "toAccountId": 2, "amount": 200 }
      responses:
        "200":
          description: Transfer operation completed successfully
          content:
            application/json:
              schema:
                type: object
                properties:
                  success: 
                    type: boolean
        "404":
          description: Account not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  success: 
                    type: boolean
                  message: 
                    type: string
        default:
          description: Unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/UnexpectedError"
      deprecated: false  
components:
  schemas:
    UnexpectedError:
      type: object
      properties:
        success:
          type: boolean
        message:
          type: string
      additionalProperties: false
tags:
  - name: deposit
    description: Deposit operation
  - name: withdraw
    description: Withdraw operation
  - name: transfer
    description: Transfer operation`