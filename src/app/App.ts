import express, { Express, Request, Response } from "express";
import { IncomingMessage, Server, ServerResponse } from "http";
import Withdraw from "../routes/withdraw";
import Deposit from "../routes/deposit";
import Transfer from "../routes/transfer";
import bodyParser from "body-parser";
import { parse } from "yaml";
import { readFile } from "fs/promises";
import * as OpenApiValidator from "express-openapi-validator";
import swaggerUi from "swagger-ui-express";
import { spec } from "../app/spec";

export class App {
  public readonly express: Express = express();
  private readonly port: Number;
  private server: Server | undefined;

  constructor(port: number) {
    this.port = port;
    this.express.use(bodyParser.json());
    const swaggerDocument = parse(spec, { prettyErrors: false });

    this.express.use(
      "/api-docs",
      swaggerUi.serve,
      swaggerUi.setup(swaggerDocument),
    );
    this.express.use(
      OpenApiValidator.middleware({
        apiSpec: swaggerDocument,
        validateRequests: true,
        validateResponses: false,
        validateSecurity: true,
      }),
    );
  }

  start(): void {

    this.express.get("/dummy", (request: Request, response: Response) => {
      response.send("something!");
    });

    this.express.use("/withdraw", Withdraw);
    this.express.use("/deposit", Deposit);
    this.express.use("/transfer", Transfer);

    this.express.use((err: Error, req: Request, res: Response) => {
      return res.status(500).json({
        succes: false,
        message: "Internal server error",
      });
    });

    this.server = this.express.listen(this.port);
  }

  stop(): void {
    this.server?.close();
  }
}
